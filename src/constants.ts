

export const LOG = {
    GETALL: {
        doing: "get all users",
        doingFlag: 0
    },
    GETBYPID: {
        doing: "get user by pid : ",
        doingFlag: 1
    },
    CREATE: {
        doing: "create user",
        doingFlag: 2
    },
    UPDATE: {
        doing: "edit user information pid : ",
        doingFlag: 3
    },
    DELETE: {
        doing: "Delete user pid : ",
        doingFlag: 4
    },
    LOGIN: {
        doing: "login",
        doingFlag: 5
    },
    SEND5FORM: {
        doing: "send form from user",
        doingFlag: 6
    }
}

export const API = '/api/v1'

export const HTTP_VERBS = {
    POST: "POST",
    GET: "GET",
    PUT: "PUT",
    DELETE: "DELETE"
}

export const RESONSE_STATUS = {
    CREATE_SUCCESS: {
        status: "ok",
        statusCode: 201,
        message: "created"
    },
    UPDATE_SUCCESS: {
        status: "ok",
        statusCode: 202,
        message: "updated"
    }
}

