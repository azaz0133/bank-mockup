import { ServerHapi } from './server';
import { createConnection } from 'typeorm';
import { allRoutes } from './routes';
import { swaggerOptions } from './utils/apidocumentation';
import { authChecking } from './utils/auth';
import { arrPermission } from './utils/enums';


class Main {
    private server: ServerHapi;

    constructor() {
        this.server = new ServerHapi({
            host: process.env.HOST || "localhost",
            port: process.env.PORT || "8081"
        });
    }

    public startServer = async (): Promise<any> => {
        const launch = this.server.initial();
        await createConnection()
        try {
            await launch.register([
                {
                    plugin: require("hapi-authorization"),
                    options: {
                        roles: arrPermission
                    }
                },
                require("hapi-auth-jwt2"),
                require('inert'),
                require('vision'),
                {
                    plugin: require("hapi-swagger"),
                    options: swaggerOptions

                }
            ])

            launch.auth.strategy("jwt", "jwt", {
                key: process.env.SECRET_KEY || "test",
                validate: authChecking,
                verifyOptions: {
                    algorithms: process.env.ALGO || ["HS256"]
                }
            })

            launch.route(
                allRoutes
            );
        
            await launch.start();
            console.log(`Server running at --> ${launch.info.uri}`);
        } catch (error) {
            console.log(error.message);
        }

    }
}

new Main().startServer();
