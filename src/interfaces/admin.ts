import { rolesType } from "../utils/enums";



export interface ICreateAdmin {

    role: rolesType
    fname: string
    lname: string
    birthdate: string
    pid: string
    username: string
    password: string
    laserCode: string
}

export interface IUpdateAdmin {
    role?: rolesType
    fname?: string
    lname?: string
    birthdate?: string
    pid?: string
    // username: string
    // password: string
    laserCode?: string
}

export interface IGetByAdmin {
    pid?: string
    username?: string
}
