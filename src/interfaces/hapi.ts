import * as Hapi from 'hapi'

export interface IRoute extends Hapi.ServerRoute {

}

export interface IRequest extends Hapi.Request {

}

export interface IResponse extends Hapi.ResponseToolkit {

}

export interface IReturn extends Hapi.ResponseObject {

}