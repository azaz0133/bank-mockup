import {MigrationInterface, QueryRunner} from "typeorm";

export class test1553091557723 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("CREATE TABLE `e_user` (`_id` int NOT NULL AUTO_INCREMENT, `pid` varchar(255) NOT NULL, `fname` varchar(255) NOT NULL, `lname` varchar(255) NOT NULL, `username` varchar(255) NOT NULL, `password` varchar(255) NOT NULL, `laserCode` varchar(255) NOT NULL, `birthdate` varchar(255) NOT NULL, `useFlag` tinyint NOT NULL DEFAULT 1, `bornFrom` varchar(255) NOT NULL, `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `role_id` int NULL, UNIQUE INDEX `IDX_c8ec0efd2c36df80f3b343f17b` (`pid`), UNIQUE INDEX `IDX_444c354fccad4f084fabeaeb2d` (`username`), PRIMARY KEY (`_id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `e_authorization` (`_id` int NOT NULL AUTO_INCREMENT, `role` enum ('user', 'admin', 'superAdmin') NOT NULL, PRIMARY KEY (`_id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `e_logging` (`_id` int NOT NULL AUTO_INCREMENT, `doing` varchar(255) NOT NULL, `doingFlag` int NOT NULL, `useFlag` tinyint NOT NULL DEFAULT 1, `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `who_id` int NULL, PRIMARY KEY (`_id`)) ENGINE=InnoDB");
        await queryRunner.query("ALTER TABLE `e_user` ADD CONSTRAINT `FK_146e901d9e0fadd8212bf33146c` FOREIGN KEY (`role_id`) REFERENCES `e_authorization`(`_id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `e_logging` ADD CONSTRAINT `FK_c1d61ded5732003f1476e1d50df` FOREIGN KEY (`who_id`) REFERENCES `e_user`(`_id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `e_logging` DROP FOREIGN KEY `FK_c1d61ded5732003f1476e1d50df`");
        await queryRunner.query("ALTER TABLE `e_user` DROP FOREIGN KEY `FK_146e901d9e0fadd8212bf33146c`");
        await queryRunner.query("DROP TABLE `e_logging`");
        await queryRunner.query("DROP TABLE `e_authorization`");
        await queryRunner.query("DROP INDEX `IDX_444c354fccad4f084fabeaeb2d` ON `e_user`");
        await queryRunner.query("DROP INDEX `IDX_c8ec0efd2c36df80f3b343f17b` ON `e_user`");
        await queryRunner.query("DROP TABLE `e_user`");
    }

}
