import { EAuthorization } from './Authorization';
import { Entity, Column, CreateDateColumn, UpdateDateColumn, PrimaryGeneratedColumn, PrimaryColumn, ManyToOne } from 'typeorm';
import { ROLES, rolesType } from '../utils/enums';


@Entity()
export class EUser {

    @PrimaryGeneratedColumn()
    _id?: number;

    @Column({
        unique: true,
    })
    pid?: string

    @Column()
    fname?: string;

    @Column()
    lname?: string

    @Column({
        unique: true
    })
    username?: string

    @Column({
        select: false
    })
    password?: string

    @Column({
        nullable: false
    })
    laserCode?: string

    @Column({
        nullable: false
    })
    birthdate?: string


    @Column({
        default: true
    })
    useFlag?: boolean

    @Column({
        nullable: false
    })
    bornFrom?: string

    @ManyToOne(type => EAuthorization, AU => AU.role)
    role?: EAuthorization




    @CreateDateColumn()
    createdAt?: Date

    @UpdateDateColumn()
    updatedAt?: Date

}