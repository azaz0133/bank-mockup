import { EUser } from './../../models/User';
import { MLogging } from './../logging/model';
import { authorBy } from './../../utils/authorize';
import { LOG, RESONSE_STATUS } from './../../constants';
import { IRequest, IResponse, IReturn } from './../../interfaces/hapi';
import { MAdmin } from './model';
import * as Boom from 'boom'
import { ICreateAdmin, IUpdateAdmin } from '../../interfaces/admin';
import { IUserJwt } from '../../interfaces/user';




export class CAdmin {

    private model = new MAdmin()

    public create = async (
        req: IRequest,
        h: IResponse
    ): Promise<IReturn | Boom> => {

        const doing = LOG.CREATE.doing
        const token = req.headers.authorization
        let user: any
        if (token) {
            user = await authorBy(token)
            LOG.CREATE.doing = LOG.CREATE.doing + req.payload['pid']
            new MLogging().create(LOG.CREATE, user)
            LOG.CREATE.doing = doing
        } else {
            return Boom.unauthorized("Have no authorization token")
        }

        try {
            const result = await this.model.create(
                req.payload as ICreateAdmin,
                user as IUserJwt
            )
            return h.response({
                ...RESONSE_STATUS.CREATE_SUCCESS,
                result
            }).code(201)
        } catch (error) {
            return Boom.badRequest(error.message)
        }

    }

    public update = async (
        req: IRequest,
        h: IResponse
    ): Promise<IReturn | Boom> => {

        const doing = LOG.UPDATE.doing
        const token = req.headers.authorization
        const user: any = await authorBy(token)
        LOG.UPDATE.doing = LOG.UPDATE.doing + req.payload['pid']
        new MLogging().create(LOG.UPDATE, user)
        LOG.UPDATE.doing = doing

        try {
            const result = await this.model.update(
                req.payload as IUpdateAdmin,
                user as IUserJwt
            )

            return h.response({
                ...RESONSE_STATUS.UPDATE_SUCCESS,
                result
            }).code(202)
        } catch (error) {
            return Boom.badRequest(error.message)
        }

    }

    public destroy = async (
        req: IRequest,
        h: IResponse
    ): Promise<IReturn | Boom> => {

        const { pid }: any = req.params
        const doing = LOG.DELETE.doing
        const token = req.headers.authorization
        LOG.DELETE.doing = LOG.DELETE.doing + pid
        const user: any = await authorBy(token)
        new MLogging().create(LOG.DELETE, user)
        LOG.DELETE.doing = doing

        try {
            const result = await this.model.update({
                pid
            }, user as IUserJwt, true)
            return h.response({
                statusCode: 202,
                status: "ok",
                message: "deleted user id : " + pid,
                result
            })
        } catch (error) {
            return Boom.badRequest(error.message)
        }

    }

    // public get = async (
    //     req: IRequest,
    //     h: IResponse
    // ): Promise<IReturn | Boom> => {

    // }

    // public getByPid = async (
    //     req: IRequest,
    //     h: IResponse
    // ): Promise<IReturn | Boom> => {

    // }

}