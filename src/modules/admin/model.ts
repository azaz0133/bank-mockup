import { UBcrypts } from './../../utils/bcrypt';
import { ICreateAdmin, IUpdateAdmin } from "../../interfaces/admin";
import { IUserJwt } from "../../interfaces/user";
import { getConnection } from "typeorm";
import { EUser } from "../../models/User";
import { EAuthorization } from "../../models/Authorization";
import { checkLevelRole, checkOrigin } from "../../utils/role";
import { rolesType } from '../../utils/enums';


export class MAdmin {


    public create = (
        attrs: ICreateAdmin,
        user: IUserJwt
    ) => new Promise(async (resolve, reject) => {
        try {
            if ((user.role == "superAdmin")
                ||
                user.role == "admin"
            ) {
                if (!checkLevelRole(user.role, attrs.role)) {
                    reject({
                        message: "you can't create user which have permission more than you"
                    })
                }
                const admin = this.findAdminIdentity(user)
                const role = this.findAuthorization(attrs.role)
                const readyGetData = await Promise.all([admin, role])
                if (readyGetData[0].role.role != user.role) {
                    /* not change data in jwt if possible */
                    reject({
                        message: "something wrong in your token role doesn't match"
                    })
                }

                const arrAuthors = readyGetData[0].bornFrom.split(",")
                arrAuthors.push(user.username)
                const value: EUser = {
                    ...attrs,
                    role: readyGetData[1],
                    bornFrom: arrAuthors.toString()
                }

                const encodePassword = new UBcrypts().encode(attrs.password)
                value.password = encodePassword
                const { identifiers } = await getConnection().createQueryBuilder()
                    .insert().into(EUser)
                    .values([value]).execute()

                const primaryKeyCreated = identifiers[0]._id

                resolve(primaryKeyCreated)
            }
        } catch (error) {
            reject(error)
        }
    })

    public update = (
        attr: IUpdateAdmin = {},
        user: IUserJwt,
        isDelete: boolean = false
    ) => new Promise(async (resolve, reject) => {

        try {
            const userUpdate = await getConnection().getRepository(EUser).findOne({
                where: {
                    pid: attr.pid,
                    useFlag: true
                }
            })

            if (!userUpdate) reject({
                message: "not found this user"
            })

            if (
                checkOrigin(
                    user.username,
                    userUpdate.bornFrom.split(",")
                ) ||
                user.role == "superAdmin"
            ) {
                if (attr.role != undefined) {
                    attr.role = await this.findAuthorization(attr.role) as any
                }
                const pid = attr['pid']
                delete attr["pid"] /* avoid admin chainge pid */
                if (isDelete) {
                    const { raw } = await getConnection().createQueryBuilder().update(EUser).set({
                        useFlag: false
                    }).where(`pid = :pid`, {
                        pid
                    }).andWhere("useFlag = 1").execute()
                    if (raw.changedRows == 0) reject({
                        message: `not found user id  : ${pid}`
                    })
                    resolve()
                }
                const { raw } = await getConnection().createQueryBuilder().update(EUser).set(attr as any).where(`pid = :pid`, {
                    pid
                }).andWhere("useFlag = 1").execute()
                if (raw.changedRows == 0) reject({
                    message: `not found user id  : ${pid}`
                })

                resolve(raw)
            } else {
                reject({
                    message: "you can't edit or change something which user is not create from you"
                })
            }
        } catch (error) {
            reject(error)
        }
    })

    private findAuthorization = (role: rolesType) => getConnection().getRepository(EAuthorization).findOne({
        where: {
            role
        }
    })

    private findAdminIdentity = (user: IUserJwt) => getConnection().getRepository(EUser).findOne({
        where: {
            username: user.username,
            pid: user.pid,
            useFlag: true,
        },
        relations: ["role"]
    })

}