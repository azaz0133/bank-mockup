import { CAdmin } from './controller';
import { IRoute } from "../../interfaces/hapi";
import { HTTP_VERBS, API } from "../../constants";
import { adminValidation } from './validation';




export const adminRoutes: Array<IRoute> = [
    {
        path: `${API}/admin/create`,
        method: HTTP_VERBS.POST,
        options: {
            auth: "jwt",
            plugins: {
                "hapiAuthorization": {
                    roles: ["admin", "superAdmin"]
                }
            },
            validate: {
                payload: adminValidation.create.payload,
                headers: adminValidation.common.headers,
                options: {
                    allowUnknown:true
                }
            },
            tags: ["api", "admin"],
            handler: new CAdmin().create
        }
    },
    {
        path: `${API}/admin/update`,
        method: HTTP_VERBS.POST,
        options: {
            auth: "jwt",
            plugins: {
                "hapiAuthorization" : {
                    roles: ["admin","superAdmin"]
                }
            },
            validate: {
                options: {
                    allowUnknown:true
                },
                headers: adminValidation.common.headers,
                payload: adminValidation.update.payload
            },
            tags: ["api","admin"],
            handler: new CAdmin().update
        }
    },
    {
        path: `${API}/admin/delete/{pid}`,
        method: HTTP_VERBS.DELETE,
        options: {
            auth: "jwt",
            plugins: {
                "hapiAuthorization" : {
                    roles: ["admin","superAdmin"]
                }
            },
            validate: {
                options: {
                    allowUnknown:true
                },
                headers: adminValidation.common.headers,
                params: adminValidation.destroy.params
            },
            tags: ["api","admin"],
            handler: new CAdmin().destroy
        }
    }
]