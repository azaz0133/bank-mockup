import * as Joi from 'joi'
import { arrPermission } from '../../utils/enums';



export const adminValidation = {

    common: {
        headers: Joi.object({
            Authorization: Joi.string().token().optional()
        })
    },

    create: {
        payload: Joi.object({
            role: Joi.string().valid([...arrPermission]).required().example(arrPermission[0]),
            fname: Joi.string().required().example("foo"),
            lname: Joi.string().required().example("boo"),
            pid: Joi.string().max(13).min(13).required().example("1100501439614"),
            username: Joi.string().regex(/^[_A-z0-9]*((-|\s)*[_A-z0-9])*$/).example("azaz0133"),
            password: Joi.string().required().example("testtest").min(8).description("more than 7 chars >= 8"),
            laserCode: Joi.string().optional().example("sasada-asdas"),
            birthdate: Joi.string().optional().example(new Date().toDateString())
        })
    },

    update: {
        payload: Joi.object({
            role: Joi.string().valid([...arrPermission]).optional().example(arrPermission[0]),
            fname: Joi.string().optional().example("foo"),
            lname: Joi.string().optional().example("boo"),
            pid: Joi.string().max(13).min(13).required().example("1100501439614"),
            laserCode: Joi.string().optional().example("sasada-asdas"),
            birthdate: Joi.string().optional().example(new Date().toDateString())
        })
    },

    destroy: {
        params: {
            pid: Joi.string().max(13).min(13).required().example("1100501439614"),
        }
    }

}

