import { UJwt } from './../../utils/jwt';
import { UBcrypts } from './../../utils/bcrypt';
import { MUser } from './../user/model';
import { IRequest, IResponse, IReturn } from './../../interfaces/hapi';
import * as Boom from 'boom'

export class CAuth {

    public login = async (
        req: IRequest,
        h: IResponse
    ): Promise<IReturn | Boom> => {
        const { username, password }: any = req.payload

        try {
            const result = await new MUser().getByUsername(username)
            const verify = new UBcrypts().decode(password, result.hash)

            if (!verify) {
                return Boom.unauthorized("password is wrong")
            }
            delete result["hash"]
            const token = new UJwt().generateToken(result)
            return h.response({
                statusCode: 201,
                status: "ok",
                message: "login already"
            }).header("Authorization", `Bearer ${token}`).code(201)


        } catch (error) {
            console.log(error);
            return Boom.badRequest(error.message)
        }


    }

}