import { authValidate } from './validation';
import { CAuth } from './controller';
import { API, HTTP_VERBS } from './../../constants';
import { IRoute } from "../../interfaces/hapi";



export const authRoute: Array<IRoute> = [
    {
        path: `${API}/login`,
        method: HTTP_VERBS.POST,
        options: {
            validate: {
                payload: authValidate.login.payload
            },
            handler: new CAuth().login,
            tags: ["api", "authen"]
        }
    }
]