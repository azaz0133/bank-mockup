import { MLogging } from './model';
import { IRequest, IResponse, IReturn } from './../../interfaces/hapi';
import * as Boom from 'boom'

export class CLogging {

    private model = new MLogging()

    public getLog = async (
        req: IRequest,
        h: IResponse
    ): Promise<IReturn | Boom> => {

        const { type }: any = req.params


        try {
            const results = await this.model.get(type)

            return h.response({
                statusCode: 200,
                status: "ok",
                message: "get log",
                results
            })
        } catch (error) {
            return Boom.badRequest(error.message)
        }


    }

}