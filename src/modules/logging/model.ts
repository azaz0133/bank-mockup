import { EUser } from './../../models/User';
import { getConnection } from 'typeorm';
import { ELogging } from '../../models/Logging';



export class MLogging {

    public create = (name: any, user: EUser) => {

        const value: ELogging = {
            doing: name.doing,
            doingFlag: name.doingFlag,
            who: user,
        }
        getConnection().createQueryBuilder().insert().into(ELogging).values([value]).execute()
    }

    public get = (doingFlag: string): Promise<Array<ELogging>> => new Promise(async (resolve, reject) => {

        const where: any = {
            useflag: true
        }

        if (!doingFlag.includes("all")) {
            where.doingFlag = doingFlag
        }

        const results = await getConnection().getRepository(ELogging).find({
            where,
            relations: ["who"]
        })

        if (results.length === 0) {
            reject("not found")
        }

        resolve(results)
    })


}