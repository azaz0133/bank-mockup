import * as Joi from 'joi'



export const VUser = {
    send5FormField: {
        payload: Joi.object({
            pid: Joi.string().required().max(13).min(13).example("1100501439614"),
            fname: Joi.string().required(),
            lname: Joi.string().required(),
            birthdate: Joi.string().required().example("29/07/1997"),
            laserCode: Joi.string().required().example("asdsds222")
        })
    }
}