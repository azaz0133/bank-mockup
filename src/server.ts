import * as Hapi from 'hapi'



export interface Init {
    port: string;
    host: string;
}

export class ServerHapi {

    public port: string;
    public host: string;

    constructor(i: Init) {
        this.port = i.port;
        this.host = i.host;
    }

    public initial = (): Hapi.Server => {
        const server = new Hapi.Server({
            host: this.host,
            port: this.port,
            routes: {
                cors: {
                    origin: ['http://localhost:8080'],
                    headers: ['Authorization', 'X-CSRF-Token', 'If-None-Match']
                }
            },
        });

        return server;
    }

    private plugins = () => {
        // this.initial().register()
    }


}