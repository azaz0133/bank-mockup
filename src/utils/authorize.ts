import { MUser } from './../modules/user/model';
import { UJwt } from './jwt';
import { EUser } from './../models/User';


export const authorBy = async (token: string): Promise<EUser> => {
    const jwt = token.match(/Bearer (.*)/)[1]
    const payload = new UJwt().decode(jwt)
    const user = await new MUser().getByPid(payload.pid)
    if (user === undefined) {
        throw new Error("wrong pid")
    }
    return user as EUser
}