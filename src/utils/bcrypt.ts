import * as bcryptjs from 'bcryptjs'

export class UBcrypts {

    public encode = (input: string): string => bcryptjs.hashSync(input, 12)

    public decode = (input: string, hash: string): boolean => bcryptjs.compareSync(input, hash)

}