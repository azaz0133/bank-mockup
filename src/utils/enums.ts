

export enum ROLES {
    user = "user",
    admin = "admin",
    superAdmin = "superAdmin"
}

export declare type rolesType = "admin" | "user" | "superAdmin"

export const arrPermission: rolesType[] = ["user", "admin", "superAdmin"]