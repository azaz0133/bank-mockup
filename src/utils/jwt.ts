import * as jwt from 'jsonwebtoken'


interface IJwt {
    pid: string,
    role?: string
}

export class UJwt {

    public generateToken = attr => jwt.sign(attr, process.env.SECRET_KEY || "test", {
        expiresIn: '1h'
    })

    public decode = (token: string): IJwt => {
        try {
            const decoed = jwt.verify(token, process.env.SECRET_KEY || "test")
            return decoed as IJwt
        } catch (error) {
            throw new Error(error)
        }
    }

}