import { rolesType, arrPermission } from "./enums";




export const chechRole = (
    role: rolesType, 
    roles: Array<rolesType>
) => {
    let isAuthor: boolean = false
    if (roles.indexOf(role) > -1) {
        isAuthor = true
    }
    return isAuthor
}



export const checkLevelRole = (
    role: rolesType, 
    roleCompare: rolesType
) => {
    let isPermission: boolean = false
    if (arrPermission.indexOf(role) >= arrPermission.indexOf(roleCompare)) {
        console.log("object");
        isPermission = true
    }
    return isPermission
}

export const checkOrigin = (
    origin: string,
    originCompare: Array<string>
): boolean => {
    let isorigin = false
    if(originCompare.indexOf(origin) > 0) {
        isorigin = true
    }
    return isorigin

}
